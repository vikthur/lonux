import ManageBusinessComponent from './components/home/business/manage_business/ManageBusinessComponent.vue'
import EditBusinessDetails from './components/home/business/manage_business/EditBusinessDetails.vue'
import BusinessHomeComponent from './components/home/business/BusinessHomeComponent.vue'
import NewBusinessComponent from './components/home/business/NewBusinessComponent.vue'
import AddNewShopComponent from './components/home/business/AddNewShopComponent.vue'
// import AddItemsToShop from './components/home/business/shops/AddItemsToShop.vue'
import RiderHomeComponent from './components/home/rider/RiderHomeComponent.vue'
import EditShop from './components/home/business/manage_business/EditShop.vue'
import NewRiderComponent from './components/home/rider/NewRiderComponent'
// import ShopIndex from './components/home/business/shops/ShopIndex.vue'
import SetupComponent from './components/home/SetupComponent.vue'
import DisplayShops from './components/home/business/DisplayShops'
import ManagersComponent from './components/home/business/ManagersComponent'
import ViewManagerComponent from './components/home/business/ViewManagerComponent'
import DisplayCategoryItems from './components/home/business/shops/DisplayCategoryItems'
import DisplayService from './components/home/business/shops/DisplayService'
import DisplayServices from './components/home/business/shops/DisplayServices'
import Statistics from './components/home/business/Statistics'
import ShopSalesDisplayPage from './components/home/business/shops/ShopSalesDisplayPage'
import SalesTable from './components/home/business/shops/SalesTable'
import OpenSaleShopIndex from './components/home/business/shops/OpenSaleShopIndex'
import ServicesOrientedShopIndex from './components/home/business/shops/ServicesOrientedShopIndex'
import AddServices from './components/home/business/shops/services_oriented/AddServices'
import AddInventory from './components/home/business/shops/AddInventory.vue'

import Underconstruction from './components/shared/Underconstruction.vue'

import overview from './components/user/overview.vue'
import Activities from './components/user/activities.vue'
import Bookings from './components/user/orders.vue'

import DisplayOrders from './components/home/business/DisplayOrders.vue'


export const routes = [
  { path: '/home/setup', components: { shop: SetupComponent } },
  { path: '/home/statistics', components: { shop: Statistics } },
  { path: '/home/shop/services/add/:shopKey', name: 'AddItems', components: { shop: AddServices } },
  { path: '/home/add_new_shop', components: { shop: AddNewShopComponent } },
  { path: '/home/new_business', components: { shop: NewBusinessComponent } },
  { path: '/home/business_home', components: { shop: BusinessHomeComponent } },
  { path: '/home/shop/open-sale/:shopKey', name: 'Saleshop', components: { shop: OpenSaleShopIndex } },
  { path: '/home/shop/service-oriented/:shopKey', name: 'ServiceShop', components: { shop: ServicesOrientedShopIndex } },
  { path: '/home/shops', components: { shop: DisplayShops } },
  { path: '/home/shop_managers', components: { shop: ManagersComponent } },
  { path: '/home/view_manager/:managerId', name: 'ViewManagerComponent', components: { shop: ViewManagerComponent } },
  { path: '/home/display/:catId/:catName', name: 'DisplayCategoryItems', components: { shop: DisplayCategoryItems } },
  { path: '/home/service/display/:id/:service', name: 'DisplayService', components: { shop: DisplayService } },
  { path: '/home/service/display/services', name: 'DisplayServices', components: { shop: DisplayServices }, props: true },
  { path: '/home/shop_display_page/:key', name: 'ShopSalesDisplayPage', components: { shop: ShopSalesDisplayPage } },
  { path: '/home/sales_table/:key', name: 'SalesTable', components: { shop: SalesTable } },
  { path: '/home/add_inventory/:key', name: 'addInventory', components: {shop: AddInventory }},

  { path: '/home/orders', components: { shop: DisplayOrders } },

  // managing business
  { path: '/home/manage_business', components: { shop: ManageBusinessComponent } },
  { path: '/home/manage_business/edit_business', components: { shop: EditBusinessDetails } },
  { path: '/home/manage_business/edit_shop/:shopKey', components: { shop: EditShop } },

  // riders routes
  { path: '/home/rider_home', components: { shop: RiderHomeComponent } },
  { path: '/home/rider_new', components: { shop: NewRiderComponent } },
  { path: '/home/*', component: { template: '<h1>Page Not Found!!!</h1>' } },

  // user dashboard

  { path: '/user/home/dashboard', components: { user: overview } },
  { path: '/user/home/activities', components: { user: Activities } },
  { path: '/user/home/bookings', components: { user: Bookings } },
  { path: '/user/home/*', components: { user: overview } },
  
  { path: '/underconstruction/:pageTitle', name: 'underconstruction', components: { shop: Underconstruction } },
  



]
