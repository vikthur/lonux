/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
// vuex
import Vuex from 'vuex'

import VueRouter from 'vue-router'

import Toasted from 'vue-toasted';

import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-default.css'

import lightBoxCSS from 'vue-image-lightbox/dist/vue-image-lightbox.min.css'

import SweetModal from 'sweet-modal-vue/src/plugin.js'

import VueLazyLoad from 'vue-lazyload'

import { routes } from './routes'
import actions from "./vuex/actions"
import mutations from "./vuex/mutations"
import state from "./vuex/state"
import getters from './vuex/getters';
import mixins from "./mixin"

require('./bootstrap')

window.Vue = require('vue')
Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueToast)
Vue.use(SweetModal)
Vue.use(VueLazyLoad)
Vue.use(lightBoxCSS)
Vue.use(Toasted)

// routes
const router = new VueRouter({
  routes: routes,
  mode: 'history'
})

// vuex store
const store = new Vuex.Store({
  state,
  getters,
  actions, 
  mutations
})

// eventbus
export const lonuxbus = new Vue()


// global mixins
Vue.mixin({
  ...mixins
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


// Vue.component('a-sidenav', require('./components/admin/shared/sidenavComponent').default);
// Vue.component('a-topnav', require('./components/admin/shared/TopnavComponent').default);

Vue.component('phone-component', require('./components/auth/PhoneNumberVerification.vue').default)
Vue.component('register-component', require('./components/auth/RegisterComponent').default)
Vue.component('login-component', require('./components/auth/LoginComponent.vue').default)
Vue.component('reset-password', require('./components/auth/PasswordReset').default)
Vue.component('home-component', require('./components/home/HomeComponent.vue').default)
Vue.component('welcome-component', require('./components/welcome.vue').default)
Vue.component('search', require('./components/search/search.vue').default)
Vue.component('result-component', require('./components/store/result.vue').default)
Vue.component('phone-component', require('./components/auth/PhoneNumberVerification.vue').default)
Vue.component('register-component', require('./components/auth/RegisterComponent').default)
Vue.component('login-component', require('./components/auth/LoginComponent.vue').default)
Vue.component('home-component', require('./components/home/HomeComponent.vue').default)
Vue.component('welcome-component', require('./components/welcome.vue').default)
Vue.component('search', require('./components/search/search.vue').default)
Vue.component('result-component', require('./components/store/result.vue').default)
Vue.component('cart', require('./components/shared/modals/Cart.vue').default)
Vue.component('cart-page', require('./components/user/CartPage.vue').default)

// user dashboard
Vue.component('user-dashboard', require('./components/user/dashboard.vue').default)
Vue.component('shop-page', require('./components/search/shop_page.vue').default)
Vue.component('orders-component', require('./components/user/orders.vue').default)
Vue.component('directions-component', require('./components/user/directions.vue').default)

// misc

Vue.component('search-field', require('./components/shared/SearchField.vue').default)

/**
 *
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
  el: '#app',
  router: router,
  store: store,
})
