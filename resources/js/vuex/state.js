const state = {
    cart: [],
    isLoggedIn: false,
    enableSideBar: false,
    GlobalColor: '#fd961a',
    locationData: {},
    page: {
      title: ''
    },
    query: 'person',
    user: {}
}

module.exports = state