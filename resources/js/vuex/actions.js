// import axios from 'axios'

const actions = {
    checkAuth: ({commit}) => {
        const val = window.lonux.isLoggedIn
        commit('setLogginStatus', val)
    },
    clearCart: ({ commit }) => {
        commit('emptyCart')
    },
    addToCartAction: ({ commit }, item) => {
        commit('setCart', item)
    },
    fetchCart: ({commit}) => {
        let cart = JSON.parse(localStorage.getItem('cart'))
        if (cart) {
            commit('syncCart', cart)
        }
    },
    changeQuantity: ({ commit }, data) => {
        commit('updateItemQuantity', data)
    },
    removeItem: ({ commit }, id) => {
        commit('removeCartItem', id)
    },
    saveUserLocationAction({commit}, data){
        commit('saveLocationData', data)
    },
    fetchLocation({commit}){
        let loc = JSON.parse(localStorage.getItem('locationData'))
        if (loc) {
            commit('syncLocation', loc)
        }
    },
    setAuthAction({commit}, user){
        if (user) {
            commit('setAuth', user)
        }
    }
}

module.exports = actions