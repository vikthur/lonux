const mutations = {
    setLogginStatus: (state, val) => {
        state.isLoggedIn = val
    },
    emptyCart: (state) => {
        state.cart = []
        localStorage.removeItem('cart')
    },
    removeCartItem: (state, id) => {
        const cartItems = state.cart.filter(cartItem => {
          return cartItem.shop_item_id != id
        })
  
        state.cart = cartItems
  
        localStorage.removeItem('cart')
        localStorage.setItem("cart", JSON.stringify(state.cart))
    },
    updateItemQuantity: (state, data) => {
      console.log(data)
        const item = state.cart.find(cartItem => {
          return cartItem.shop_item_id == data.id
        })
  
        if (data.text == 'increase') {
          item.quantity++
        } else {
          if (item.quantity == 1) return
          item.quantity--
        }
  
        localStorage.removeItem('cart')
        localStorage.setItem("cart", JSON.stringify(state.cart))
    },
    syncCart: (state, cart) => {
        state.cart = cart
    },
    setCart: (state, item) => {
        const cartItem = state.cart.find(cartItem => {
          return cartItem.shop_item_id == item.shop_item_id
        })
        if (cartItem) {
          cartItem.quantity++
          localStorage.setItem("cart", JSON.stringify(state.cart))
          return
        }
        state.cart = [...state.cart, item]
  
        localStorage.setItem("cart", JSON.stringify(state.cart))
    },
    saveLocationData(state, loc){
        state.locationData = loc
		localStorage.setItem("locationData", JSON.stringify(state.locationData))
    },
	syncLocation(state, loc){
		state.locationData = loc
	},
	setAuth(state, user) {
		state.user = user,
		state.isLoggedIn = true
	}
}

module.exports = mutations
