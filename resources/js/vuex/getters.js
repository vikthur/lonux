const getters = {
    cartItems: (state) => state.cart,
    loginStatus: (state) => state.isLoggedIn,
    locationData: (state) => state.locationData,
    user: (state) => state.user
}

module.exports = getters