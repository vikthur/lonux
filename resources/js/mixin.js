const mixin = {
    data () {
        return {
          pathOrigin: window.location.origin,
          enableSideBar: false,
          loader: {
            color: '#fd961a',
            loading: false
          },
        }
    },
    computed: {
        has_setup: function () {
          if (window.lonux.has_setup == true) {
            return true
          }
          return false
        },
        account_type: function () {
          if (typeof window.lonux.accType !== 'undefined') {
            return window.lonux.accType
          } else {
            return null
          }
        },
        company: function () {
          if (window.lonux.company) {
            return window.lonux.company
          } else {
            return null
          }
        },
        hasShops: function () {
          if (window.lonux.shopsCount > 0) {
            return true
          } else {
            return false
          }
        },
        firstName: function () {
          return window.lonux.name.split(' ')[0]
        }
    },
    methods: {
		showToast(msg, type){
			this.$toasted.show(msg, {
                type: type,
                duration: 2000
            })
		},
        getImage(val){
          if (val) return val.split(',')[0]
    
          return "https://lonux.s3.us-east-2.amazonaws.com/dummy.png"
       },
        disable(el) {
          $(el).attr('disabled', true)
          this.loader.loading = true
        },
        enable(el) {
          $(el).attr('disabled', false)
          this.loader.loading = false
        },
        distance(lat1, lon1, lat2, lon2, unit) {
          if ((lat1 == lat2) && (lon1 == lon2)) {
              return 0;
          }
          else {
              var radlat1 = Math.PI * lat1/180;
              var radlat2 = Math.PI * lat2/180;
              var theta = lon1-lon2;
              var radtheta = Math.PI * theta/180;
              var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
              if (dist > 1) {
                  dist = 1;
              }
              dist = Math.acos(dist);
              dist = dist * 180/Math.PI;
              dist = dist * 60 * 1.1515;
              if (unit=="K") { dist = dist * 1.609344 }
              if (unit=="N") { dist = dist * 0.8684 }
              return Math.floor(dist);
          }
      }
    },
    filters: {
        formatCurrency(value){
          const formatter = new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'NGN' });
    
          return formatter.format(value)
        },
        convertToKM(value){
          return (value / 1000).toFixed(2) + "KM away"
        },
  	}
}

module.exports = mixin