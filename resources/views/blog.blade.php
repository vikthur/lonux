@extends('layouts.app')

@section('title')

Blog

@endsection

@section('styles')
    <style>
    .get-started-btn{
        color: #f6891f;
        border-bottom: 1px solid #d3d3d3;
    }
    </style>
@endsection

@section('content')

    <section class="lonux-blog blog ">
        
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9">
                
                    <div class="blog-content ">

                            <h1 class="blog-title">Getting started for businesses</h1> 
                            <div class="blog-featured-image w-100">
                                <figure>
                                    <img src="{{asset('/img/shop.svg')}}" class="img-fluid" alt="Featured image">
                                    {{-- <figcaption class="text-center">Image caption</figcaption> --}}
                                </figure>
                            </div>   
                            <div class="blog-meta d-flex">
                                <span class="d-inlin-block mr-4 ">July 28, 2021</span>
                                <span class="d-inlin-block mr-4 ">|</span>
                                <span class="d-inlin-block  ">1 min read</span>

                            </div>            
                            <div class="blog-post text-justify mt-5">
                                <p>
                                    Are you a business owner with quality items, a shop, making sales, or struggling to make sales? Do you possess skills that can render quality services to customers? 
                                </p>
                                <p>
                                    Well, read on, this is for you.
                                </p>
                                <p>
                                    We want to introduce you to LONUX. <br/>
                                    Lonux is a platform that offers unlimited business possibilities. We bring your business to the world of abut 1.3 Million online shoppers and to physical shoppers in Abuja.<br>
                                    We help strategize and organize your business ready for your potential buyers. We give you tools to make managing your business profitable, easy, and simple.
                                </p>
                                <p>
                                    Are you ready to jump on this moving world of business?
                                </p>
                                <p class="blog-post text-center mt-5">
                                    <a href="/register" class="btn btn-default get-started-btn">Get Started Now</a>
                                </p>

                                <p>
                                    Read more about <a target='blank' href='https://blog.lonux.com.ng/2021/06/09/lonux-for-business-owners/'>Lonux for Businesses</a>
                            </div>

                    </div>
                </div>
            </div>
        
        </div>
    </section>

@endsection