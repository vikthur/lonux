<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <title>Lonux - Buy fast. Sell more</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
        content="Get what you are looking for faster. Increase your visibility and sell your products with ease. Earn money making deliveries" />

    <meta property="og:site_name" content="Lonux" />
    <meta property="og:title" content="Buy fast. Sell more" />
    <meta property="og:description"
        content="Get what you are looking for faster. Increase your visibility and sell your products with ease. Earn money making deliveries" />
    <meta property="og:url" content="{{ \URL::to('/') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ asset('img/LONUX-logo.jpg') }}" />
    <meta property="og:image:secure_url" content="{{ asset('img/LONUX-logo.jpg') }}" />
    {{-- <meta property="og:image:width" content="1280" /> --}}
    {{-- <meta property="og:image:height" content="640" /> --}}
    <meta property="twitter:card" content="summary_large_image" />
    <meta property="twitter:image" content="{{ asset('img/LONUX-logo.jpg') }}" />
    <meta property="twitter:site" content="Lonux" />


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- favicon --}}
    <link rel="shortcut icon" href="{{ asset('img/lonux-icon.svg') }}" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

</head>
<style>
    .typed-element {
        display: block !important;
    }

</style>

<body>
    <div id="app" class="welcome">


        <nav class="navbar navbar-expand-sm bg-dark nav-lonux">
            <div class="container">
                <ul class="nav ml-auto">
                    @guest
                        <li class="nav-item">
                            <a class="nav-link lonux-link" href="https://twitter.com/lonuxng">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M12 0.480011C5.63758 0.480011 0.47998 5.63761 0.47998 12C0.47998 18.3624 5.63758 23.52 12 23.52C18.3624 23.52 23.52 18.3624 23.52 12C23.52 5.63761 18.3624 0.480011 12 0.480011ZM16.686 9.91681C16.6908 10.0152 16.692 10.1136 16.692 10.2096C16.692 13.2096 14.4108 16.6668 10.2372 16.6668C9.00393 16.6689 7.79634 16.3147 6.75958 15.6468C6.93598 15.6684 7.11718 15.6768 7.30078 15.6768C8.36398 15.6768 9.34198 15.3156 10.1184 14.706C9.64524 14.6967 9.18679 14.54 8.80691 14.2578C8.42704 13.9756 8.14468 13.5819 7.99918 13.1316C8.33897 13.1962 8.689 13.1827 9.02278 13.092C8.50924 12.9882 8.04742 12.7099 7.71563 12.3044C7.38384 11.8989 7.20251 11.3911 7.20238 10.8672V10.8396C7.50838 11.0088 7.85878 11.112 8.23078 11.124C7.74934 10.8035 7.40852 10.3112 7.27803 9.74777C7.14755 9.18432 7.23724 8.59231 7.52878 8.09281C8.09873 8.79361 8.80949 9.36691 9.61506 9.77558C10.4206 10.1843 11.303 10.4192 12.2052 10.4652C12.0905 9.97838 12.1399 9.4673 12.3456 9.01142C12.5513 8.55553 12.9019 8.1804 13.3429 7.94433C13.7838 7.70826 14.2904 7.62448 14.7839 7.70602C15.2773 7.78756 15.73 8.02984 16.0716 8.39521C16.5794 8.29474 17.0664 8.10848 17.5116 7.84441C17.3423 8.37017 16.988 8.81662 16.5144 9.10081C16.9642 9.04663 17.4034 8.9257 17.8176 8.74201C17.5134 9.19788 17.1301 9.59574 16.686 9.91681Z" />
                                </svg>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link lonux-link" href="https://facebook.com/lonuxng">
                                <svg width="26" height="26" viewBox="0 0 26 26" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M13.0011 2.16882C7.01891 2.16882 2.16882 7.01891 2.16882 13.0011C2.16882 18.4069 6.12949 22.8876 11.3089 23.7033V16.133H8.55724V13.0011H11.3089V10.6145C11.3089 7.89749 12.9263 6.39924 15.3996 6.39924C16.5847 6.39924 17.8262 6.61049 17.8262 6.61049V9.27441H16.4569C15.1136 9.27441 14.6932 10.1107 14.6932 10.9677V12.9989H17.6952L17.2152 16.1308H14.6932V23.7012C19.8727 22.8897 23.8333 18.408 23.8333 13.0011C23.8333 7.01891 18.9832 2.16882 13.0011 2.16882Z" />
                                </svg>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link lonux-link " href="/login">Sign in for businesses</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link lonux-link" href="/home">Business</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link lonux-link" href="/home/user">Customer</a>
                        </li>
                    @endguest
                </ul>

            </div>
        </nav>

        <div class=" search-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9">

                        <img src="/img/lonux-logo-01.svg" class="welcome-logo" alt="lonux logo">


                        <div class="form position-relative">
                            <div class="form-group form-search-lg">
                                <input type="text" id="search-input" name="searchBtn" class="form-control form-zero"
                                    placeholder="search an item. eg ceiling fan" required autocomplete="off">
                                {{-- <button type="submit" class="form-zero">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M15.5 14H14.71L14.43 13.73C15.41 12.59 16 11.11 16 9.5C16 5.91 13.09 3 9.5 3C5.91 3 3 5.91 3 9.5C3 13.09 5.91 16 9.5 16C11.11 16 12.59 15.41 13.73 14.43L14 14.71V15.5L19 20.49L20.49 19L15.5 14ZM9.5 14C7.01 14 5 11.99 5 9.5C5 7.01 7.01 5 9.5 5C11.99 5 14 7.01 14 9.5C14 11.99 11.99 14 9.5 14Z"
                                            fill="#F5F5F4" />
                                    </svg>
                                </button> --}}
                            </div>

                            {{-- <transition name="fade">
                                <div class="search-section-suggestion" style="display: none">
                                    <ul>
                                        <li>
                                            <button href="#" type="submit"
                                                class="search-section-suggestion-item"></button>
                                        </li>
                                    </ul>
                                </div>
                            </transition> --}}
                        </div>
                    </div>
                </div>
                <span class="arrow-down-link">
                    <a href="#info-section">
                        <svg width="48" height="48" viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg">
                            <path d="M40 24L37.18 21.18L26 32.34V8H22V32.34L10.84 21.16L8 24L24 40L40 24Z" />
                        </svg>
                    </a>
                </span>


            </div>


        </div>
        <div class="info-section info-section-1" id="info-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <h2 class="text-center">Sell your product</h2>
                        <p class="text-center">Become more visible to customers. LONUX creates a space that enables
                            vendors to be easily found by customers searching for their product or service. </p>

                        <img src="{{ asset('img/buy.svg') }}" alt="">
                    </div>
                </div>
                <div class="row justify-content-center mt-5">
                    <a href="/register" style="background-color: #3f3d56; border-color: #3f3d56; color: white;"
                        class="btn btn-primary mt-5">Get Started for Businesses</a>
                </div>
            </div>
        </div>
        <div class=" info-section info-section-2">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <h2 class="text-center">Get what you are looking for faster</h2>
                        <p class="text-center">Stop time wasting! Stop stressing! LONUX Makes it easy for you to
                            quickly find and buy products or services from the nearest providers to you. </p>

                        <img src="{{ asset('img/shop.svg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class=" info-section info-section-3">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <h2 class="text-center">Earn money, making deliveries</h2>
                        <p class="text-center">Do you own a motor bike?
                            Become a Dispatch rider.
                            Get steady flow of work...

                            Help vendors deliver products to customers and get paid for it. </p>

                        <img src="{{ asset('img/job.svg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer bg-dark">
            <div class="container">
                <p class="text-center text-white">copyright &copy; <span id="getCurrentDate">2020</span></p>
            </div>
        </footer>

        <div class="modal fade bd-example-modal-lg" id="infoLaunchModal" tabindex="-1" role="dialog"
            aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-body mt-5">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-4 col-md-2">
                                    <img src="{{ 'img/lonux-icon.svg' }}" class="img-fluid lonuxLoadingIcon"
                                        alt="Lonux Icon" />
                                </div>
                            </div>
                            <div class="row justify-content-center mt-5">
                                <div class="col-8 col-md-8">
                                    <h4>We are still indexing</h4>
                                    <p>Want to buy that item really fast and easy right ? We love to help you achieve
                                        that. With Businesses already indexing their products with us, we will soon
                                        bring you the <em>Comfort and Ease in Buying</em> that you deserve.</p>
                                    <p>Business owner ? <a href="/register">Get Started</a></p>
                                </div>
                            </div>
                            <div class="row justify-content-center mt-5">
                                <div class="col-md-8">
                                    <h4>Follow Us For Updates</h4>
                                    <ul class="nav ml-auto">
                                        <li class="nav-item">
                                            <a class="nav-link lonux-link" target="blank"
                                                href="https://facebook.com/lonuxng">
                                                <img
                                                    src="https://img.icons8.com/ios-glyphs/30/000000/facebook-new.png" />
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link lonux-link" target="blank"
                                                href="https://twitter.com/lonuxng">
                                                <img
                                                    src="https://img.icons8.com/material/30/000000/twitter-squared.png" />
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link lonux-link" target="blank"
                                                href="https://instagram.com/lonuxng">
                                                <img
                                                    src="https://img.icons8.com/ios-glyphs/30/000000/instagram-new.png" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('bootstrap/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    {{-- <script>
        $('#search-input').click(() => {
            $('div#infoLaunchModal').modal({
                show: true,
            })
        })
    </script> --}}


    <!-- [ ... ] -->
    <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.jquery.min.js"></script>
    <script>

        function getImage(value) {
            if (value) {
                const arr = value.split(',')

                console.log(arr, value)

                return arr[0];
            }
            return 'https://lonux.s3.us-east-2.amazonaws.com/dummy.png'
        }

        function formatPrice(value){
           const formatter = new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'NGN' });

           return formatter.format(value)
        }

        var client = algoliasearch('KXTSLO4TQ3', 'f13957d8159b58bc968b20b97c1c1a45')
        var index = client.initIndex('Lonux_items');
        //var index = client.initIndex('devitems');
        $('#search-input').autocomplete({
            hint: false
        }, [{
            source: $.fn.autocomplete.sources.hits(index, {
                hitsPerPage: 5
            }),
            displayKey: 'item_name',
            templates: {
                suggestion: function(suggestion) {
                    console.log(suggestion)
                    //return suggestion._highlightResult.item_name.value;

                    const markup = `<div class="row">
                        <div class="col-4">
                            <img src="${getImage(suggestion.images)}" class="algolia-thumb">
                        </div>
                        <div class="col-8 text-left">
                            <h6 style="font-weight: 900">${suggestion.item_name} - ${suggestion.item_type == 'shop_item' ? 'item sale' : 'services'}</h6>
                            <p class="text-muted">${formatPrice(suggestion.item_cost)}</p>
                        </div>
                    </div>`
                    //const markup = `
                    // <div class="algolia-results">
                    //    <img src="http://www.thebeancounter.com/wp-content/uploads/2015/08/for_dummies_plain.png" class="algolia-thumb">
                    //     <p class="ml-3">
                    //        <p class="subtitle">${suggestion._highlightResult.item_name.value}</p><hr/>
                    //     </p>
                    //     <p class="text-small ml-0">NGN ${suggestion._highlightResult.item_cost.value}</p>
                    // </div>
                    //`
                    return markup
                },
                empty: function(result) {
                    return `Sorry, lonux does not have <strong>${result.query}</strong> at this time`
                }
            }
        }]).on('autocomplete:selected', function(event, suggestion, dataset, context) {
            window.location.href = `${window.location.origin}/search?q=${suggestion.item_name}&type=${suggestion.item_type}`
            // console.log(suggestion.item_name);
        });

        var year = new Date().getFullYear()
        var el = document.getElementById('getCurrentDate')
        el.innerHTML = year;

        function runSearch(e) {
            e.preventDefault()
            console.log(document.getElementById('search-input').value)
        }
    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/60ff46a3d6e7610a49ad1e85/1fbiinsl9';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

    <style>
        .algolia-autocomplete {
            width: 100%;
        }

        .algolia-autocomplete .aa-input,
        .algolia-autocomplete .aa-hint {
            width: 100%;
        }

        .algolia-autocomplete .aa-hint {
            color: #999;
        }

        .algolia-autocomplete .aa-dropdown-menu {
            width: 100%;
            background-color: #fff;
            border: 1px solid #999;
            border-top: none;
            position: relative;
            padding: 0;
            top: 100%;
            left: 0;
        }

        .algolia-autocomplete .aa-dropdown-menu .aa-suggestion {
            cursor: pointer;
            padding: 5px 4px;
        }

        .algolia-autocomplete .aa-dropdown-menu .aa-suggestion.aa-cursor {
            background-color: #B2D7FF;
        }

        .algolia-autocomplete .aa-dropdown-menu .aa-suggestion em {
            font-weight: bold;
            font-style: normal;
        }

         .algolia-results{
            text-align: left !important;
            display: flex;
            justify-content: left !important;
            align-items: center !important;
        }
        .algolia-thumb{
            max-width: 100%;
            max-height: 55px;
        } 

        .lonuxLoadingIcon {
            -webkit-animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @-moz-keyframes spin {
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

    </style>
</body>

</html>
