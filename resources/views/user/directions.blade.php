@extends('layouts.app')

@section('title')
	Get Directions
@endsection

@section('content')

<directions-component code={{$code}}></directions-component>

@endsection