@extends('layouts.app')

@section('title')

search result

@endsection

@section('content')

<result-component :shop="{{$shop}}"></result-component>

@endsection