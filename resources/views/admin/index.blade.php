@extends('layouts.admin')

@section('content')
    <h3 class="mb-4">Overview</h3>
    <div class="row">
        <div class="col-md-3">
            <div class="card overview-card">
                <div class="card-header">
                    <span>No. of users</span>
                </div>
                <div class="card-body">
                    <span class="h2">225</span>
                </div>
                <!-- <div class="card-footer">
                    <span class="font-weight-bold btn">
                        Last 6 months
                    </span>
                </div> -->
            </div>
        </div>
        <div class="col-md-3">
            <div class="card overview-card">
                <div class="card-header">
                    <span>No. of Managers</span>
                </div>
                <div class="card-body">
                    <span class="h2">32</span>
                </div>
               
            </div>
        </div>
        <div class="col-md-3">
            <div class="card overview-card">
                <div class="card-header">
                    <span>No of shops</span>
                </div>
                <div class="card-body">
                    <span class="h2">40</span>
                </div>
               
            </div>
        </div>
        <div class="col-md-3">
            <div class="card overview-card">
                <div class="card-header">
                    <span>Total Inventories</span>
                </div>
                <div class="card-body">
                    <span class="h2">5421</span>
                </div>
               
            </div>
        </div>
    </div>
@endsection