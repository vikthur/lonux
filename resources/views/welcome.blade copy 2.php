
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <title>Lonux - Welcome</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- favicon --}}
    <link rel="shortcut icon" href="{{asset('img/lonux-icon.svg')}}" type="image/x-icon">
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
</head>
<style>
    .typed-element{
        display: block !important;
    }
</style>
<body>
    <div id="app" class="welcome">
   
    
        <nav class="navbar navbar-expand-sm bg-dark nav-lonux">
            <div class="container">
                <ul class="nav ml-auto">
                    @guest
                    <li class="nav-item">
                        <a class="nav-link lonux-link" href="/login">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 0.480011C5.63758 0.480011 0.47998 5.63761 0.47998 12C0.47998 18.3624 5.63758 23.52 12 23.52C18.3624 23.52 23.52 18.3624 23.52 12C23.52 5.63761 18.3624 0.480011 12 0.480011ZM16.686 9.91681C16.6908 10.0152 16.692 10.1136 16.692 10.2096C16.692 13.2096 14.4108 16.6668 10.2372 16.6668C9.00393 16.6689 7.79634 16.3147 6.75958 15.6468C6.93598 15.6684 7.11718 15.6768 7.30078 15.6768C8.36398 15.6768 9.34198 15.3156 10.1184 14.706C9.64524 14.6967 9.18679 14.54 8.80691 14.2578C8.42704 13.9756 8.14468 13.5819 7.99918 13.1316C8.33897 13.1962 8.689 13.1827 9.02278 13.092C8.50924 12.9882 8.04742 12.7099 7.71563 12.3044C7.38384 11.8989 7.20251 11.3911 7.20238 10.8672V10.8396C7.50838 11.0088 7.85878 11.112 8.23078 11.124C7.74934 10.8035 7.40852 10.3112 7.27803 9.74777C7.14755 9.18432 7.23724 8.59231 7.52878 8.09281C8.09873 8.79361 8.80949 9.36691 9.61506 9.77558C10.4206 10.1843 11.303 10.4192 12.2052 10.4652C12.0905 9.97838 12.1399 9.4673 12.3456 9.01142C12.5513 8.55553 12.9019 8.1804 13.3429 7.94433C13.7838 7.70826 14.2904 7.62448 14.7839 7.70602C15.2773 7.78756 15.73 8.02984 16.0716 8.39521C16.5794 8.29474 17.0664 8.10848 17.5116 7.84441C17.3423 8.37017 16.988 8.81662 16.5144 9.10081C16.9642 9.04663 17.4034 8.9257 17.8176 8.74201C17.5134 9.19788 17.1301 9.59574 16.686 9.91681Z"/>
                            </svg>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link lonux-link" href="/login">
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M13.0011 2.16882C7.01891 2.16882 2.16882 7.01891 2.16882 13.0011C2.16882 18.4069 6.12949 22.8876 11.3089 23.7033V16.133H8.55724V13.0011H11.3089V10.6145C11.3089 7.89749 12.9263 6.39924 15.3996 6.39924C16.5847 6.39924 17.8262 6.61049 17.8262 6.61049V9.27441H16.4569C15.1136 9.27441 14.6932 10.1107 14.6932 10.9677V12.9989H17.6952L17.2152 16.1308H14.6932V23.7012C19.8727 22.8897 23.8333 18.408 23.8333 13.0011C23.8333 7.01891 18.9832 2.16882 13.0011 2.16882Z" />
                            </svg>

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link lonux-link " href="/login">sign in</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link lonux-link" href="/auth/phone">sign up</a>
                    </li> -->
                    @else
                    <li class="nav-item">
                        <a class="nav-link lonux-link" href="/home">Dashboard</a>
                    </li>
                    @endguest
                </ul>
            
            </div>
        </nav>

        <div class="search-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9">
    
                        <img src="/img/lonux-logo-01.svg"  class="welcome-logo" alt="lonux logo">
    
                        {{-- timer goes here  --}}
                        {{-- <p class="text-white">Item search starts in</p>
                        <div class="row" style="color:wheat">
                            <div class="col-3">
                                <h4 id="days">00</h4>
                                <h6 style="font-weight: bolder;">days</h6>
                            </div>
                            <div class="col-3">
                                <h4 id="hrs">00</h4>
                                <h6 style="font-weight: bolder;">hours</h6>
                            </div>
                            <div class="col-3">
                                <h4 id="mins">00</h4>
                                <h6 style="font-weight: bolder;">minutes</h6>
                            </div>
                            <div class="col-3">
                                <h4 id="secs">00</h1>
                                <h6 style="font-weight: bolder;">seconds</h6>
                            </div>
                        </div> --}}

                        {{-- <h2 class="text-white mt-5">Join the wait list...</h2>
                        <form class="form position-relative" method="post" action="{{route('waitlist')}}">
                            @csrf
                            <div class="form-group form-search-lg">
                                <input type="text" name="contact" class="form-control form-zero" placeholder="Email or Phone" autofocus required autocomplete="off">
                                <button type="submit" class="form-zero">
                                    <span class="text-white text-bold">Join</span>
                                </button>
                            </div>
                        </form>
                        @if (session('saved'))
                            <div class="alert alert-success">
                                {{session('saved')}}
                            </div>
                        @endif

                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <p class="text-danger">{{$error}}</p>
                            @endforeach
                        @endif --}}
                        

                        <form class="form position-relative">
                            <div class="form-group form-search-lg">
                                <input type="text" id="search-input" name="searchBtn" class="form-control form-zero" placeholder="search an item. eg ceiling fan" autofocus required autocomplete="off">
                                <button type="submit" class="form-zero">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M15.5 14H14.71L14.43 13.73C15.41 12.59 16 11.11 16 9.5C16 5.91 13.09 3 9.5 3C5.91 3 3 5.91 3 9.5C3 13.09 5.91 16 9.5 16C11.11 16 12.59 15.41 13.73 14.43L14 14.71V15.5L19 20.49L20.49 19L15.5 14ZM9.5 14C7.01 14 5 11.99 5 9.5C5 7.01 7.01 5 9.5 5C11.99 5 14 7.01 14 9.5C14 11.99 11.99 14 9.5 14Z" fill="#F5F5F4"/>
                                    </svg>
                                </button>
                            </div>
                            
                            <transition name="fade" >
                                <div class="search-section-suggestion" style="display: none">
                                    <ul>
                                        <li>
                                            <button href="#" type="submit" class="search-section-suggestion-item"></button>
                                        </li>
                                    </ul>	 
                                </div>
                            </transition>
                        </form>

                        {{-- <p class="text-center text-primary welcome-title mb-3">
                            <vue-typed-js :fadeOut="true" :loop="true" :strings="['Easy <span class=text-lead>selling,</span> Fast <span class=text-lead>shopping,</span> Instant <span class=text-lead>jobs.</span>']">
                              <span class="typing"></span>
                            </vue-typed-js>
                        </p> --}}
                        {{-- <h3 class="font-weight-bold text-white mb-2 mt-5">Are you looking for a product and need it closer to you?</h3> --}}
                        <h3 class="font-weight-bold text-white mb-2 mt-5" style="display:block">
                            <vue-typed-js :fadeOut="true" :loop="true" :strings="['Are you looking for a Product and need it closer to you ? Search ', 'Are you a Business Owner? Become more visible to Customers. ', 'Do you own a Motor Bike? Become a Dispatch Rider. ']">
                                <span class="typing"></span>
                              </vue-typed-js>
                        </h3>
                        <p class="text-light font-italic mb-4" style="display:block">Easy Selling, Fast Shopping, Jobs.</p>
                         
    
                        <!--<p class="mt-4 text-light">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora neque magni amet molestiae aperiam et corrupti voluptates minima sit omnis! </p> -->
                        <div class="text-center" style="display:block"><a href="#getStarted" class="btn btn-primary">Start using Lonux</a></div>
                    </div>
                </div>
    
              
            </div>
    
    
        </div>
        
        <div class="welcome-extra bg-primary" id="getStarted">
            <div class="container">
                <div class="row welcome-extra-cards">
                    <div class="col-lg-4">
                        <div class="welcome-extra-card">
                            {{-- <img :src="pathOrigin + '/img/buy.svg'" alt="" class="welcome-extra-card-img"> --}}
                            <h5>Get what you are looking for faster</h5>
                            <p>Stop time wasting! Stop stressing! LONUX Makes it easy for you to quickly find and buy products or services from the nearest providers to you.</p>
                            <a href="#" class="font-weight-bold btn disabled">Get started</a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="welcome-extra-card">
                            {{-- <img :src="pathOrigin + '/img/shop.svg'" alt="" class="welcome-extra-card-img"> --}}
                            <h5>Sell your products with ease</h5>
                            <p>Become more visible to customers. LONUX creates a space that enables vendors to be easily found by customers searching for their product or service.</p>
                            <a href="/auth/phone" class="font-weight-bold">Get started</a>

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="welcome-extra-card">
                            {{-- <img :src="pathOrigin + '/img/job.svg'" alt="" class="welcome-extra-card-img"> --}}
                            <h5>Become a worker instantly</h5>
                            <p>Do you own a motor bike? Become a Dispatch Rider. Get steady flow of work. Make Pickups from vendors and deliver to customers closest to you and get paid handsolmy.</p>
                            <a href="#" class="font-weight-bold btn disabled">Get started</a>

                        </div>
                    </div>
                </div>
                
                <div class="welcome-extra-grand">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-7">
                                    <h2 class="text-center font-weight-bold mb-4">
                                        It's a win for everyone
                                    </h2>
                                    <p class="text-center"> Customers buy faster, Dispatch Riders get Paid, Businesses make more sales.</p>
                                    <div class="text-center">
                                        <a href="/auth/phone" class="btn btn-dark">Get Started for Businesses</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <footer class="footer mb-2 p-0">
                    <div class="container">
                        <p class="text-center">
                            &copy; Lonux <span id="getCurrentDate"></span>
                        </p>
                    </div>
                </footer>
            </div>
        </div>
     
    </div>

    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('bootstrap/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>

    <!-- [ ... ] -->
    <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.jquery.min.js"></script>
    <script> 
        var client = algoliasearch('KXTSLO4TQ3', 'f13957d8159b58bc968b20b97c1c1a45')
        var index = client.initIndex('Lonux_PROD_dev');
        $('#search-input').autocomplete({ hint: false }, [
            {
            source: $.fn.autocomplete.sources.hits(index, { hitsPerPage: 5 }),
            displayKey: 'item_name',
            templates: {
            suggestion: function(suggestion) {
              return suggestion._highlightResult.item_name.value;
            // const markup = `
            //     <div class="algolia-results">
            //         <span>
            //             <img src="http://www.thebeancounter.com/wp-content/uploads/2015/08/for_dummies_plain.png" class="algolia-thumb">
            //             ${suggestion._highlightResult.item_name.value}
            //         </span>
            //         <span>NGN ${suggestion._highlightResult.item_cost.value}</span>
            //     </div>
            //     <div class="algolia-details">
            //         <span>This is the details of the item</span>
            //     </div>
            // `
            // return markup
            },
            empty: function(result){
                return `Sorry, lonux does not have <strong>${result.query}</strong> at this time`
            }
          }
        }
        ]).on('autocomplete:selected', function(event, suggestion, dataset, context) {
            window.location.href = `${window.location.origin}/search?q=${suggestion.item_name}`
            // console.log(suggestion.item_name);
        });

        var year = new Date().getFullYear()
        var el = document.getElementById('getCurrentDate')
        el.innerHTML = year;

        // TIMER
        function timer() {
            var dys = document.getElementById('days')
            var hrs = document.getElementById('hrs')
            var mins = document.getElementById('mins')
            var secs = document.getElementById('secs')

            // Set the date we're counting down to
            var countDownDate = new Date("Sept 30, 2021 00:00:00").getTime();
            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Update respective divs
            dys.innerHTML = days < 10 ? '0'+days: days 
            hrs.innerHTML = hours < 10 ? '0'+hours : hours
            mins.innerHTML = minutes < 10 ? '0'+minutes : minutes
            secs.innerHTML = seconds < 10 ? '0'+seconds : seconds

        }

        setInterval(() => { timer() }, 1000);

    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/60ff46a3d6e7610a49ad1e85/1fbiinsl9';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->

    <style>
        .algolia-autocomplete {
            width: 100%;
        }
        .algolia-autocomplete .aa-input, .algolia-autocomplete .aa-hint {
            width: 100%;
        }
        .algolia-autocomplete .aa-hint {
            color: #999;
        }
        .algolia-autocomplete .aa-dropdown-menu {
            width: 100%;
            background-color: #fff;
            border: 1px solid #999;
            border-top: none;
            position: absolute;
            padding: 0;
            top: 100%;
            left: 0;
        }
        .algolia-autocomplete .aa-dropdown-menu .aa-suggestion {
            cursor: pointer;
            padding: 5px 4px;
        }
        .algolia-autocomplete .aa-dropdown-menu .aa-suggestion.aa-cursor {
            background-color: #B2D7FF;
        }
        .algolia-autocomplete .aa-dropdown-menu .aa-suggestion em {
            font-weight: bold;
            font-style: normal;
        }
        /* .algolia-results{
            display: flex;
            justify-content: space-between;
            align-items: center;
        }
        .algolia-details{
            text-align: left;
        }
        .algolia-thumb{
            max-width: 35px;
            max-height: 35px;
        } */
    </style>
 </body>
</html>