<!DOCTYPE html>
<html style="background-color: #eee">

<head>
    <meta charset="UTF-8">

    <title>Lonux - @yield('title')</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description"
        content="Get what you are looking for faster. Increase your visibility and sell your products with ease. Earn money making deliveries" />

    <meta property="og:site_name" content="Lonux" />
    <meta property="og:title" content="Buy fast. Sell more" />
    <meta property="og:description"
        content="Get what you are looking for faster. Increase your visibility and sell your products with ease. Earn money making deliveries" />
    <meta property="og:url" content="{{ \URL::to('/') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ asset('img/LONUX-logo.jpg') }}" />
    <meta property="og:image:secure_url" content="{{ asset('img/LONUX-logo.jpg') }}" />
    {{-- <meta property="og:image:width" content="1280" /> --}}
    {{-- <meta property="og:image:height" content="640" /> --}}
    <meta property="twitter:card" content="summary_large_image" />
    <meta property="twitter:image" content="{{ asset('img/LONUX-logo.jpg') }}" />
    <meta property="twitter:site" content="Lonux" />

    {{-- favicon --}}
    <link rel="shortcut icon" href="{{ asset('img/lonux-icon.svg') }}" type="image/x-icon">


    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

    {{-- <link rel="stylesheet" href="css/style"> --}}

    <!-- icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    {{-- //init user details for vue usage --}}
    <script>
        window.lonux = {
            "has_setup": "{{ lonuxUser()->has_setup }}",
            "company": "{{ lonuxUser()->company ? lonuxUser()->company->name : null }}",
            "accType": "{{ lonuxUser()->accType ? lonuxUser()->accType->name : null }}",
            "name": "{{ lonuxName() }}",
            "shopsCount": "{{ count(lonuxUser()->shops) }}",
            'csrf_token': "{{ csrf_token() }}",
            'userId': "{{ lonuxId() }}",
            'rider': "{{ lonuxUser()->rider ? lonuxUser()->rider : null }}",
            'user': "{{ Auth::user() }}"
        }
    </script>
</head>

<body style="background-color: #eee">
    <div id="app">

        @yield('content')

    </div>
    <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
