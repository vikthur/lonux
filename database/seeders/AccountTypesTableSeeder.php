<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AccountTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('account_types')->insert(
        	[
	        	[
	            'name' => 'Business Account',
	        	],
	        	[
	            'name' => 'Rider Account',
	        	],
	        	[
	            'name' => 'Basic Account',
	        	],
        	]
    	);
    }
}
