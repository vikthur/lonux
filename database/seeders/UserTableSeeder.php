<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Lonux\User::class, 200)->create()->each(function ($user) {
        	$user->company()->save(factory(\Lonux\Company::class)->make());
    	});
    }
}
