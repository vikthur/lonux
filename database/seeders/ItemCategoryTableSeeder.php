<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ItemCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('item_categories')->insert(
        	[
	        	[
                    'item' => 'Fruits',
                    'shop_id' => 1
	        	],
	        	[
                    'item' => 'Snacks',
                    'shop_id' => 2
	        	],
	        	[
                    'item' => 'Drugs',
                    'shop_id' => 3
                ],
                [
                    'item' => 'Cloths',
                    'shop_id' => 4
                ],
                [
                    'item' => 'Soft Drinks',
                    'shop_id' => 5
                ],
        	]
    	);
    }
}
