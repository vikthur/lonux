<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
        	UserTableSeeder::class,
        	AccountTypesTableSeeder::class,
        	ShopTableSeeder::class,
        	ShopTypeTableSeeder::class,
            ShopServiceTableSeeder::class,
            ShopItemsTableSeeder::class,
            ItemCategoryTableSeeder::class,
            // ItemTableSeeder::class
        ]);
    }
}
