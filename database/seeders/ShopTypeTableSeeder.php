<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ShopTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Lonux\ShopType::class, 20)->create();
    }
}
