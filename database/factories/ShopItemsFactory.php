<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Lonux\ShopItems;


$factory->define(ShopItems::class, function (Faker $faker) {
    $faker->addProvider(new \FakerRestaurant\Provider\en_US\Restaurant($faker));
    return [
        'name' => $faker->randomElement([$faker->foodName(),$faker->beverageName(),$faker->dairyName(),$faker->vegetableName(),$faker->fruitName(),$faker->meatName(),$faker->sauceName()]),
        'amount' => $faker->numberBetween(100, 500),
        'available_in_stock' => $faker->numberBetween(7,50),
        'pieces_available' => $faker->numberBetween(7,50),
        'sold_in' => $faker->randomElement(['wholesale','retail']),
        'shop_id' => $faker->numberBetween(1, 500),
        'item_category_id' => $faker->numberBetween(1, 5),
        'upc' => $faker->ean13(),
    ];
});
