<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_stats', function (Blueprint $table) {
            $table->id();
            $table->integer('shop_id');
            $table->integer('user_id')->nullable();
            $table->ipAddress('client_ip');
            $table->text('user_agent');
            $table->string('search_term');
            $table->integer('search_app_pos');
            $table->boolean('was_visited')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_stats');
    }
}
