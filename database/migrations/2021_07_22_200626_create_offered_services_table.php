<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferedServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offered_services', function (Blueprint $table) {
            $table->id();
            $table->string('shop_key');
            $table->string('service_name');
            $table->string('service_duration');
            $table->string('service_charge');
            $table->boolean('service_availability')->default(0);
            $table->boolean('deliverable')->default(0);
            $table->text('service_images')->nullable();
            $table->text('service_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offered_services');
    }
}
