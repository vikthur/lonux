<?php

namespace Lonux\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Lonux\AccountTypes;
use Lonux\ShopService;

class lonux_init extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lonux_init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init some tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // shop service table
        $this->info('Setting up shop services table...');

        $check = ShopService::all();

        if (count($check)) {
            $this->line('Shop service table already created');
        } else {
            $create = DB::table('shop_services')->insert([
                [
                    'name' => 'Open Sales',
                    'count' => 0,
                ],
                [
                    'name' => 'Services Oriented',
                    'count' => 0,
                ]
            ]);

            if ($create) {
                $this->info('Shop service setup successfully');
            }
        }



        // account types table 

        $this->info('Setting up account types table...');

        $checkDB = AccountTypes::all();

        if (count($checkDB)) {
            $this->line('Account types table already created');
        } else {
            $createDB = DB::table('account_types')->insert([
                [
                    'name' => 'Business Account',
                ],
                [
                    'name' => 'Rider Account',
                ],
                [
                    'name' => 'Basic Account',
                ]
            ]);

            if ($createDB) {
                $this->info('Account types table created successfully');
            }
        }

        return;
    }
}
