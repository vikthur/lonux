<?php

namespace Lonux;

use Illuminate\Database\Eloquent\Model;

class OfferedService extends Model
{
    public function sales()
    {
        return $this->hasMany('Lonux\Sale');
    }
}
