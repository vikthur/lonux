<?php

namespace Lonux;
use Illuminate\Support\Str;

class Company extends Model
{

    public function user()
    {
        return $this->belongsTo('Lonux\User');
    }
    public function shops()
    {
        return $this->hasMany('Lonux\Shop');
    }

    public function managers()
    {
        return $this->hasMany('Lonux\ShopManager');
    }

    public function hasBothShops()
    {
        $openSales = false;
        $services = false;

        $shops = $this->shops;

        foreach ($shops as $key => $shop) {
            if ($shop->isServiceShop()) {
                $services = true;
            }
            if ($shop->isOpenSalesShop()) {
                $openSales = true;
            }
        }

        return ($openSales && $services);
    }

    public function hasServiceShops()
    {
        $services = false;

        $shops = $this->shops;

        foreach ($shops as $key => $shop) {
            if ($shop->isServiceShop()) {
                $services = true;
            }
        }

        return $services;
    }

    public function hasOpenSaleShops()
    {
        $openSales = false;

        $shops = $this->shops;

        foreach ($shops as $key => $shop) {
            if ($shop->isOpenSalesShop()) {
                $openSales = true;
            }
        }

        return $openSales;
    }

    public function companyData()
    {
        $shops = $this->shops;

        $totalSales = 0;
        $totalManagers = 0;
        $totalInventory = 0;

        foreach ($shops as $key => $shop) {
            $sales = $shop->sales;
            foreach ($sales as $key => $sale) {
                $totalSales = $totalSales + $sale->quantity;
            }

            $totalManagers = $totalManagers + count($shop->managers);
            $totalInventory = $totalInventory + count($shop->items);
        }
        $data = [
            "totalSales" => $totalSales,
            "totalManagers" => $totalManagers,
            "totalInventory" => $totalInventory,
            "totalShops" => count($this->shops),
            "data" => $this
        ];

        return $data;
    }

    public function createBusiness($business_name, $business_desc, $business_address, $lat, $lng)
    {
        $this->user_id = lonuxId();
        $this->name = $business_name;
        $this->desc = $business_desc;
        $this->hq_address = $business_address;
        $this->lat = $lat;
        $this->lng = $lng;

        $email = str_replace(" ", "", ucwords($business_name));
        $email = $email . time();
        $this->email = $email;
        $this->key = (string)Str::uuid();

        $this->save();

        return $this;
    }
}
