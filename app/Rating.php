<?php

namespace Lonux;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = ['id'];
    
    public function user()
    {
        return $this->BelongsTo(User::class);
    }

    public function shop()
    {
        return $this->BelongsTo(Shop::class);
    }
}
