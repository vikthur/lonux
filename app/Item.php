<?php

namespace Lonux;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use Searchable;

    // remove on production
    public function searchableAs()
    {
        return config('scout.prefix').'_items';
    }

    public function image()
    {
        $images = explode(',', $this->images);
        return $images[0];
    }

    public function images()
    {
        $images = explode(',', $this->images);
        return $images;
    }

    public function category()
    {
        return $this->belongsTo('Lonux\ItemCategory', 'item_category_id', 'id');
    }

    public function addItem($name, $amount, $images, $type, $upc = null, $catId = null)
    {
        $item = Item::where('item_name', $name)->first();

        if (is_null($item)) {
            $item = new Item();
            $item->item_name = $name;
            $item->item_cost = $amount;
            $item->images = $images;
            $item->item_type = $type;
            $item->upc = $upc;
            $item->item_category_id = $catId;

            $item->save();
        }
    }
}
