<?php

namespace Lonux;

use Lonux\Shop;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use NotificationChannels\WebPush\HasPushSubscriptions;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ShopManager extends Authenticatable
{
    use Notifiable, HasApiTokens, HasPushSubscriptions;

    protected $guard = 'manager';

    protected $fillable = [
        'name', 'phone', 'email', 'shop_key', 'last_login','password','company_id',
    ];

    protected $hidden = [
        'password'
    ];

    public function shop () 
    {
    	return Shop::getShopByKey($this->shop_key);
    }

    public function sales()
    {
        return $this->hasMany('Lonux\Sale');
    }

    public function routeNotificationForNexmo($notification)
    {
        return $this->phone;
    }
}
