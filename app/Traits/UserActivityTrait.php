<?php

namespace Lonux\Traits;

use Lonux\UserActivity;

/**
 *
 */
trait UserActivityTrait
{
    public function saveActivity($request)
    {
        $userActivity = new UserActivity();

        $userActivity->activity_type = $request->activity_type;
        $userActivity->user_agent = $request->user_agent;
        $userActivity->activity_info = json_encode($request->activity_data);
        $userActivity->user_id = lonuxUser() ? lonuxId() : null;
        $userActivity->client_ip = $this->getUserIp();
        $userActivity->client_location = $request->client_location;

        $userActivity->save();

        return "user activity saved successfully";
    }

    public function getUserIp()
    {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}
