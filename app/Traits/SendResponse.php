<?php


namespace Lonux\Traits;


/**
 *
 */
trait SendResponse
{
  function send_response($success = true, $message =  null, $data = [], $status_code = 200)
  {
    $responseObj = [
      'success' => $success,
      'message' => $message,
      'data' => $data,
    ];

    return response($responseObj, $status_code);
  }
}
