<?php

namespace Lonux;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    public function user()
    {
        return $this->belongsTo('Lonux\User');
    }

    public function company()
    {
        return $this->belongsTo('Lonux\Company');
    }

    public function itemsCategory()
    {
        return $this->hasMany('Lonux\ItemCategory');
    }

    public function shopService()
    {
        return $this->hasOne('Lonux\ShopService', 'id', 'shop_service_id');
    }

    public function managers()
    {
        return $this->hasMany('Lonux\ShopManager', 'shop_key', 'key');
    }

    public function sales()
    {
        return $this->hasMany('Lonux\Sale');
    }

    public function items()
    {
        return $this->hasMany('Lonux\ShopItems');
    }

    public function services()
    {
        return $this->hasMany('Lonux\OfferedService', 'shop_key', 'key');
    }

    public function isServiceShop()
    {
        return $this->shop_service_id == 2 ? true : false;
    }

    public function isOpenSalesShop()
    {
        return $this->shop_service_id == 1 ? true : false;
    }

    //custome help methods
    public static function getShopByKey($key)
    {
        return self::where('key', $key)->first();
    }

    public function image()
    {
        if (is_null($this->shop_image)) 
        {
            return 'https://lonux.s3.us-east-2.amazonaws.com/14270828.jpg';
        }

        return $this->shop_image;
    }

    public function rating()
    {
        return $this->hasMany('Lonux\Rating');
    }

    public function shopRating()
    {
        $ratingArr = $this->rating->toArray();
        if (count($ratingArr)) {
            $totalRating = 0;

            foreach ($ratingArr as $key => $rating) {
                $totalRating += $rating['rating'];
            }

            $averageRating = $totalRating / count($ratingArr);
            $res = [
                'rating' => $averageRating,
                'count' => count($ratingArr)
            ];
            return $res;
        } else {
            return $res = [
                'rating' => 0,
                'count' => 0,
            ];
        }
    }

    public function item($name)
    {
       return $this->items()->where('name', $name)->latest()->first();
    }

    public function service($name)
    {
       return $this->services()->where('service_name', $name)->latest()->first();
    }
}
