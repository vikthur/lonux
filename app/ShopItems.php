<?php

namespace Lonux;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopItems extends Model
{
    use SoftDeletes;


    public function category()
    {
        return $this->belongsTo('Lonux\ItemCategory', 'item_category_id', 'id');
    }

    public function sales()
    {
        return $this->hasMany('Lonux\Sale');
    }

    public function salesCount()
    {
        $salesArr = $this->sales->toArray();
        // dd($salesArr);
        if (count($salesArr)) {
            $totalSales = 0;
            foreach ($salesArr as $key => $value) {
                $totalSales += $value['quantity'];           
            }
            return $totalSales;
        }

        return 0;
    }
    
}
