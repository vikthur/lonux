<?php

namespace Lonux;

// use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{

    protected $guarded = ['id'];

	/**
	    disable increamenting the primary key
	*/

	// public $incrementing = false;


 //    public static function boot()
 //    {
 //        parent::boot();

 //        static::creating(function($model)
 //        {
 //            $model->{$model->getKeyName()} = (string)Str::uuid();
 //        });
 //    }
}
