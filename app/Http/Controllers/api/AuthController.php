<?php

namespace Lonux\Http\Controllers\api;

use Auth;
use Lonux\User;
use Lonux\Company;
use Lonux\Phone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Lonux\Traits\SendResponse;
use Illuminate\Support\Facades\Hash;
use Lonux\Http\Controllers\Controller;

class AuthController extends Controller
{
    use SendResponse;

    // users login
    public function login(Request $request)
    {
        $login = FacadesAuth::attempt([
            'phone' => $request->phone,
            'password' => $request->password
        ], true);

        // dd($login);

        if ($login) {

            $token = lonuxUser()->createToken('auth_token')->plainTextToken;

            $data = [
                'user'  => lonuxUser(),
                'token' => $token,
            ];
            return $this->send_response(true, "login suceess", $data);
        } else {
            return $this->send_response(false, "These details do not match our records", [], 401);
        }
    }

    // managers login
    public function mLogin(Request $request)
    {
        $login = FacadesAuth::guard('manager')->attempt([
            'phone' => $request->phone,
            'password' => $request->password,
        ]);

        if ($login) {
            lonuxManager()->update([
                'last_login' => now(),
            ]);

            // Revoke all tokens...
            if (lonuxManager()->tokens) {
                lonuxManager()->tokens()->delete();
                // dd(lonuxManager()->tokens);
            }

            $token = lonuxManager()->createToken('m_auth_token')->plainTextToken;

            $data = [
                'user' => lonuxManager(),
                'shop' => lonuxManager()->shop(),
                'token' => $token
            ];

            return $this->send_response(true, 'login success', $data);
        } else {
            return $this->send_response(false, "These details do not match our records", [], 401);
        }
    }

    public function register(Request $request)
    {
        $phone = Phone::where("phone", $request->phone)->first();
        if (is_null($phone)) {
            dd($phone);
            $request->phone == null;
        }

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'unique:users'],
            'address' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
            'lat' => ['required'],
            'lng' => ['required'],
        ]);

        $emailName = str_replace(" ", "", ucwords($request->name));
        $email = $emailName;
        $email = $this->UniqueEmail($email, $emailName);

        $user = User::create([
            'email' => $email,
            'name'  => $request->name,
            'phone' => $request->phone,
            'address'   => $request->address,
            'password' => Hash::make($request->password),
            'lat' => $request->lat,
            'lng' => $request->lng,
        ]);

        if ($user) {
            FacadesAuth::guard()->login($user);

            $token = $user->createToken('auth_token')->plainTextToken;

            $response = [
                'data' => [
                    'user'  => $user,
                    'token' => $token,
                ],
                'message' => 'ok'
            ];

            return $this->send_response(true, 'registeration successful', $response, 200);
        }
    }

    public function b_register(Request $request)
    {
        $phone = Phone::where("phone", '+'.$request->phone)->first();
        if (is_null($phone)) {
            return $this->send_response(true, "No verified phone number supplied", [], 422);
        } else {
            if (!$phone->is_used) {
                return $this->send_response(true, "Unverified phone number supplied", [], 422);
            }
        }

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'unique:users'],
            'address' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
            'lat' => ['required'],
            'lng' => ['required'],
        ]);

        $emailName = str_replace(" ", "", ucwords($request->name));
        $email = $emailName;
        $email = $this->UniqueEmail($email, $emailName);

        $user = User::create([
            'email' => $email,
            'name'  => $request->name,
            'phone' => $request->phone,
            'address'   => $request->address,
            'password' => Hash::make($request->password),
            'lat' => $request->lat,
            'lng' => $request->lng,
        ]);

        if ($user) {
            FacadesAuth::guard()->login($user);

            $business = new Company();
            $business = $business->createBusiness($request->business_name, $request->business_desc, $request->address, $request->lat, $request->lng);

            $token = $user->createToken('auth_token')->plainTextToken;
            $response = [
                'data' => [
                    'user'  => $user,
                    'business' => $business,
                    'token' => $token,
                ],
                'message' => 'ok'
            ];

            return $this->send_response(true, 'registeration successful', $response, 200);
        }
    }

    public function UniqueEmail($email, $emailName)
    {
        $user = User::where("email", $email)->first();
        
        if (!is_null($user)) {
            $num = time();
            $email = $emailName . $num;
        }
        
        return $email;
    }
}
