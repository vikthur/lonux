<?php

namespace Lonux\Http\Controllers;

use Illuminate\Support\Facades\Notification;
use Lonux\Traits\UserActivityTrait;
use Lonux\Notifications\NewBooking;
use Illuminate\Http\Request;
use Lonux\Booking;
use Lonux\Shop;

class BookingController extends Controller
{

    use UserActivityTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'shopId'  => 'required',
            'itemId' => 'required',
            'json_data' => 'required|array',
            'user_activity_data' => 'required|array'
        ]);

        $booking = new Booking();
        $booking->user_id = lonuxUser() ? lonuxId() : null;
        $booking->shop_id = $request->shopId;
        $booking->shop_items_id = $request->itemId;
        $booking->item_data = json_encode($request->json_data);

        $booking->order_code = $this->generateOrderCode($request->shopId);

        $booking->save();

        // save user activity
        $request->client_location = $request->user_activity_data['location'];
        $request->user_agent = $request->user_activity_data['user_agent'];
        $request->activity_type = 'booking';
        $request->activity_data = $request->json_data;

        $this->saveActivity($request);

        //get shopManagers
        $shop = Shop::findorfail($request->shopId);
        $managers = $shop->managers;

        // now send Notification 
        Notification::send($managers, new NewBooking($booking));

        return response($booking->order_code, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateOrderCode($shopId)
    {
        $strings = '0123456789abcdefghijklmnopqrstuvwxyz@#*%$&!?><';
        $codes = Booking::where('shop_id', $shopId)->pluck('order_code')->toArray();

        $index1 = rand(0,45);
        $index2 = rand(0,45);

        $code = 'L'.$strings[$index1].'n'.$strings[$index2].'x';
        $code = strtoupper($code);

        if (in_array($code, $codes)) {
            $this->generateOrderCode($shopId);
        }

        return $code;
    }
}