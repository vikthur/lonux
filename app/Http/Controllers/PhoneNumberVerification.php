<?php

namespace Lonux\Http\Controllers;

use ErrorException;
use Lonux\User;
use Lonux\Phone;
use Twilio\Rest\Client;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class PhoneNumberVerification extends Controller
{
  use SendResponse;
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function index(){
    return view('auth.phone');
  }

  public function changePassword(Request $request)
  {
    $phoneNumber = "234".$request->phoneNumber;
    $pass = $request->password;
    $pass2 = $request->passwordConfirm;

    if(strlen($pass) < 8){
      return $this->send_response(false, "Password cannot be less than eight (8) letters", [], 403);
    }

    if($pass !== $pass2){
      return $this->send_response(false, "passwords do not match", [], 403);
    }

    $newPassword = Hash::make($pass);
    
    $user = User::where('phone', $phoneNumber)->first();

    if (is_null($user)) {
      return $this->send_response(false, 'Phone Number Not Registered', [], 404);
    }

    $user->password = $newPassword;

    $user->save();

    return $this->send_response(true, "Password Changed Successfully", [], 200);

  }

  public function sendCode($phoneNumber)
  {
    // ---------- SENDCHAMP IMPLEMENTATION ---------
    $channel = 'sms';
    $token_type = 'numeric';
    $token_length = 6;
    $expiry_day = 5;
    $customer_mobile_number = $phoneNumber;
    $meta_data = ['route' => 'non_dnd'];
    $sender = "Lonux";
    $token = env('SENDCHAMP_PUBLIC_KEY');
    $fileds = [
      'customer_mobile_number' => $customer_mobile_number,
      'expiration_time' => $expiry_day,
      'token_length' => $token_length,
      'token_type' => $token_type,
      'sender' => $sender,
      'channel' => $channel,
      'meta_data' => $meta_data
    ];

    $post_fileds = json_encode($fileds);

    try {

      $curl = curl_init();

      curl_setopt_array($curl, [
        CURLOPT_URL => "https://api.sendchamp.com/api/v1/verification/create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $post_fileds,
        CURLOPT_HTTPHEADER => [
          "Accept: application/json",
          "Authorization: Bearer ". $token,
          "Content-Type: application/json"
        ],
      ]);

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      $response = json_decode($response);
      $err = json_decode($err);

      if ($err)
      {
        throw new ErrorException($err->message);
      }

      if ($response->status == 'failed')
      {
        throw new ErrorException($response->message);
      }

      return $response;

      // return $code;
    } catch (\Throwable $th) {
      dd($th);
      throw $th;
    }
    
    // --------- TWILO IMPLEMENTATION --------
    /* Get credentials from .env */
    // $token = getenv("TWILIO_AUTH_TOKEN");
    // $twilio_sid = getenv("TWILIO_SID");
    // $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
    // $twilio = new Client($twilio_sid, $token);

    // try {
    //   $twilio->verify->v2->services($twilio_verify_sid)
    //   ->verifications
    //   ->create($phoneNumber, "sms");
    // } catch (\Throwable $th) {
    //   if(get_class($th) == 'Twilio\Exceptions\RestException')
    //   {
    //     return $this->send_response(false, 'Invalid Phone Number', [], 403);
    //   }
    // }
  }

  public function verifyCode($code, $ref)
  {
    $fields = ['verification_reference' => $ref, 'verification_code' => $code];
    $post_fileds = json_encode($fields);
    $token = env('SENDCHAMP_PUBLIC_KEY');
    try {
    $curl = curl_init();

    curl_setopt_array($curl, [
      CURLOPT_URL => "https://api.sendchamp.com/api/v1/verification/confirm",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $post_fileds,
      CURLOPT_HTTPHEADER => [
        "Accept: application/json",
        "Authorization: Bearer ". $token,
        "Content-Type: application/json"
      ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    $response = json_decode($response);

    curl_close($curl);

    if ($err) {
      throw new ErrorException($err);
    } 
    return $response;

    } catch (\Throwable $th) {
      throw $th;
    }
  }

  // public function verifyCode($code, $phoneNumber)
  // {
  //   /* Get credentials from .env */
  //   $token = getenv("TWILIO_AUTH_TOKEN");
  //   $twilio_sid = getenv("TWILIO_SID");
  //   $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
  //   $twilio = new Client($twilio_sid, $token);

  //   $verification = $twilio->verify->v2->services($twilio_verify_sid)
  //   ->verificationChecks
  //   ->create($code, array('to' => $phoneNumber));

  //   return $verification;
  // }

  public function phoneVerification(Request $request)
  {
    $phoneNumber = "+234".$request->phoneNumber;
    $checkNumber = "234".$request->phoneNumber;

    if (is_null($request->code)) {

      if($request->action == 'reset_code')
      {
        $sms_data = $this->sendCode($phoneNumber);
        return $this->send_response(true, "Code sent successfully", $sms_data, 200);
      }

      $check = User::where('phone', $checkNumber)->first();
      if (!is_null($check)) {
        return $this->send_response(false, 'Phone number already exists', [], 403);
      }

      //send code here
      $sms_data = $this->sendCode($phoneNumber);

      $phone = Phone::where('phone', $phoneNumber)->first();

      if(is_null($phone)){
        $phone = new Phone();

        $phone->phone = $phoneNumber;
        $phone->save();
      }
      return $this->send_response(true, "Code sent successfully", $sms_data, 200);

    }else {

      $phone = Phone::where('phone', $phoneNumber)->first();

      if (is_null($phone)) {
        return $this->send_response(false, "Phone number not found", [], 404);
      }

      $verification = $this->verifyCode($request->code, $request->reference);

      
      if ($verification->status == 'success') {

        if($request->action == 'reset_code')
        {
          return $this->send_response(true, "Verification success", [], 200);
        }

        $phone->is_used = true;
        $phone->save();

        return $this->send_response(true, "Verification success", [], 200);
      }else{
        return $this->send_response(false, $verification->message, [], 403);
      }
    }

  }

  public function changeNumber($phone){

    $phone = Phone::where('phone', $phone)->where('is_used', false)->first();

    if (is_null($phone)) {
      return $this->send_response(true, "Phone number change success", [], 200);
    }else{
      $phone->delete();
      return $this->send_response(true, "Phone number change success", [], 200);
    }

  }
}
