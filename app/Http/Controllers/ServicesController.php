<?php

namespace Lonux\Http\Controllers;

use Lonux\Traits\SendResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Lonux\Item;
use Lonux\OfferedService;

class ServicesController extends Controller
{
    use SendResponse;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'shopKey' => 'required',
            'service_name' => 'required',
            'service_duration' => 'required',
            'service_charge' => 'required',
            'service_availability' => 'required',
            'deliverable' => 'required'
        ]);

        $service = new OfferedService();

        $service->shop_key = $request->shopKey;
        $service->service_name = $request->service_name;
        $service->service_duration = $request->service_duration;
        $service->service_charge = $request->service_charge;
        $service->service_availability = (bool)$request->service_availability;
        $service->deliverable = (bool)$request->deliverable;
        $service->service_text = $request->service_text;

        // $service->save();

        // take care of images
        $slug = 'shop_service_images/' . Str::slug(substr($service->name, 0, 10));
        $arr = [];

        if ($request->image1) {
            $dir = $slug . '_' . time();
            $path = $this->uploadToS3($request->image1, $dir);
            array_push($arr, $path);
        }

        if ($request->image2) {
            $dir = $slug . '_' . time();
            $path = $this->uploadToS3($request->image2, $dir);
            array_push($arr, $path);
        }

        if ($request->image3) {
            $dir = $slug . '_' . time();
            $path = $this->uploadToS3($request->image3, $dir);
            array_push($arr, $path);
        }

        $service->service_images = implode(',', $arr);

        $service->save();

        $item = new Item();
        $item->addItem($service->service_name, $service->service_charge, $service->service_images, 'shop_services');

        $this->send_response(true, 'Service saved successfully', []);
    }

    public function uploadToS3($image, $dir)
    {
        $path = $image->storePublicly(
            $dir,
            's3'
        );
        $path = env('AWS_BUCKET_URL').$path;

        return $path;
    }
}
