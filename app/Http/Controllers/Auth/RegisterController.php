<?php

namespace Lonux\Http\Controllers\Auth;

use Lonux\User;
use Lonux\Phone;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;
use Illuminate\Support\Facades\Hash;
use Lonux\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    use SendResponse;
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(Request $request)
    {
        $phone = $request->phone;
        $checkNumber = "234".$request->phone;
        $phoneNumber = "+234".$request->phone;

        if (is_null($phone)) {
            return view('auth.phone');
        }

        $check = User::where('phone', $checkNumber)->first();
        if (!is_null($check)) {
            return view('auth.login');
        }

        $phoneCheck = Phone::where('phone', $phoneNumber)->first();
        if (is_null($phoneCheck)) {
            return view('auth.phone');
        }

        return view('auth.register', compact('phone'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $phone = Phone::where("phone", $data['phone'])->first();
        if (is_null($phone)) {
            $data["phone"] == null;
        }
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'unique:users'],
            'address' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    protected function registered()
    {
        return $this->send_response(true, 'registeration successful', Auth::user());
        // return response($this->redirectTo, 200);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \Lonux\User
     */
    protected function create(array $data)
    {
        $emailName = str_replace(" ", "", ucwords($data["name"]));
        $email = $emailName;
        $email = $this->UniqueEmail($email, $emailName);
        return User::create([
            'email' => $email,
            'name' => $data['name'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'password' => Hash::make($data['password']),
            'lng' => $data['lng'] || '7.39778771840',
            'lat' => $data['lat'] || '9.07772042626',
            'has_setup' => true,
        ]);
    }

    public function UniqueEmail($email, $emailName)
    {
        $user = User::where("email", $email)->first();
        
        if (!is_null($user)) {
            $num = time();
            $email = $emailName . $num;
        }
        return $email;
    }
}
