<?php

namespace Lonux\Http\Controllers\Auth;

use Lonux\Traits\SendResponse;
use Lonux\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class LoginController extends Controller
{
    use SendResponse;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * determine which credencial user is signing in with.
     *
     * @return void
     */
    public function username(){

        $username = filter_var(request()->input('email') , FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        return $username;
    }

    /**
     * login the user.
     *
     * @return void
     */
    public function login()
    {
        $login = FacadesAuth::attempt([
            $this->username() => request()->input('email'),
            'password'=> request()->input('password')], true);

        if ($login) {
            return $this->send_response(true, "login successful", FacadesAuth::user(), 200);
        }else{
            return $this->send_response(false, 'login failed', [], 403);
        }

    }

}
