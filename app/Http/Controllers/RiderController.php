<?php

namespace Lonux\Http\Controllers;

use Lonux\Rider;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class RiderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'driver_license' => 'required',
            'vehicle_name' => 'required',
            'vehicle_number' => 'required',
            'vehicle_type' => 'required',
            'date_of_birth' => 'required',
            'rider_picture' => 'required|file',
            'vehicle_picture' => 'required|file',
        ]);

        $rider  =  new Rider();

        $rider->user_id = lonuxId();
        $rider->key = (string) Str::uuid();
        $rider->driver_license = $request->driver_license;
        $rider->vehicle_name = $request->vehicle_name;
        $rider->vehicle_number = $request->vehicle_number;
        $rider->vehicle_type = $request->vehicle_type;
        $rider->date_of_birth = $request->date_of_birth;
        $rider->rider_picture = $request->rider_picture->store('/public/riders/');
        $rider->vehicle_picture = $request->vehicle_picture->store('/public/vehicles');

        $rider->save();

        return response($rider, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rider = Rider::where('user_id',$id)->first();
        if (is_null($rider)) {
            return response('not_found', 200);
        }else{
            return response($rider, 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
