<?php

namespace Lonux\Http\Controllers\manager;

use Lonux\Shop;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;
use Lonux\Http\Controllers\Controller;

class HomeController extends Controller
{
    use SendResponse;
    
    public function index ()
    {
    	$key = lonuxManager()->shop_key;
    	$shop = Shop::getShopByKey($key);
    	return view('manager.index', compact('shop'));
    }

    public function newInventory ()
    {
    	return view('manager.new_inventory');
    }

    public function manageInventory () 
    {
    	return view('manager.manage_inventory');
    }

    public function settings () 
    {
    	return view('manager.settings');
    }

    public function getCategoryItems($id, $name)
    {
        return view('manager.items_view', compact('id'));
    }

    public function showItem($id, $name) 
    {
        return view('manager.show_item', compact('id'));
    }

    public function editItem ($id, $name)
    {
        return view('manager.edit_item', compact('id'));
    } 

    public function showBooking($id, $name)
    {
        return view('manager.show_booking', compact('id'));
    }

    public function subscribeManager(Request $request)
    {
        $this->validate($request,[
            'endpoint'    => 'required',
            'keys.auth'   => 'required',
            'keys.p256dh' => 'required'
        ]);

        $manager = lonuxManager();

        $endpoint = $request->endpoint;
        $token = $request->keys['auth'];
        $key = $request->keys['p256dh'];
        
        $manager->updatePushSubscription($endpoint, $key, $token);

        return response()->json(['success' => true],200);
    }

    public function updateShopStatus(Request $request, $key)
    {
        // dd(lonuxUser());
        $shop = Shop::where('key', $key)->first();

        $shop->is_up = (int)$request->status;

        $shop->save();

        return $this->send_response(true, "shop status updated successfully");
    }

    public function getShopStatus($key)
    {
        $shop = Shop::where('key', $key)->first();
        $status = $shop->is_up;
        return $this->send_response(true, "shop status retrieved successfully", $status);
    }
}
