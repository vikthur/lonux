<?php

namespace Lonux\Http\Controllers;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use Lonux\Traits\SendResponse;
use Illuminate\Http\Request;
use Lonux\Jobs\SendSalesMessage;
use Lonux\Order;
use Lonux\Pickup;
use Lonux\Shop;

class OrderController extends Controller
{
    use SendResponse;

    public function index()
    {
        if (request()->wantsJson())
        {
            $orders = lonuxUser()->orders()->orderBy('created_at', 'desc')->get()->groupBy('code');

            $return_array = [];

            foreach ($orders as $key => $order) {
                $array = [
                    'code' => $key,
                    'orders' => $order,
                    'pickup' => Pickup::where('order_code', $key)->first()
                ];

                array_push($return_array, $array);
            }

            return $this->send_response(true, 'orders retrieved successfully', $return_array);
        }
        return view('user.orders');
    }

    public function saveOrder($orders, $code)
    {
        foreach ($orders as $key => $newOrder) {
            $order = new Order();

            $data = ["name" => $newOrder['name'], "quantity" => $newOrder['quantity'], "amount" => $newOrder['amount'], "lat" => $newOrder['lat'], "lng" => $newOrder['lng'], "images" => $newOrder['images'], "shop_name" => $newOrder['shop_name']];

            $order->user_id = lonuxId();
            $order->shop_id = $newOrder['shop_id'];
            $order->shop_items_id = $newOrder['shop_item_id'];
            $order->item_data = json_encode($data);
            $order->code = $code;

            $order->save();

            // dispatch sms job
            $shop = Shop::where('id', $newOrder['shop_id'])->with('user')->first();
            $userPhone = [$shop->user->phone];
            $text = "Dear user, your shop, ". $newOrder['shop_name'] ." has a new order from LONUX with the following details: \n\n Item name: ".$newOrder['name'].".". "\n Quantity: ".$newOrder['quantity'];

            SendSalesMessage::dispatch($userPhone, $text);
        }

        return "Orders saved successfuly";
    }

    public function savePickup($code)
    {
        $pickup = new Pickup();
        $pickup->start_time = now();
        $pickup->order_code = $code;

        $pickup->save();

        return "Pickup started successfuly";
    }

    public function store(Request $request)
    {
        $orders = request()->all();
        $code = IdGenerator::generate(['table' => 'orders', 'length' => 10, 'field' => 'code', 'prefix' => 'LNX-']);

        $this->saveOrder($orders, $code);

        $this->savePickup($code);

        return $this->send_response(true, 'action successful', $code);
    }

    public function storeOrder()
    {
        $orders = request()->all();
        $code = IdGenerator::generate(['table' => 'orders', 'length' => 10, 'field' => 'code', 'prefix' => 'LNX-']);

        $saveOrder = $this->saveOrder($orders, $code);

        return $this->send_response(true, $saveOrder, []);
    }

    public function getDirections($code)
    {
        $pickup = Pickup::where('order_code', $code)->first();
        if (is_null($pickup))
        {
            $this->savePickup($code);
        }
        return view('user.directions', compact('code'));
    }

    public function show($code)
    {
        $orders = Order::where('code', $code)->get()->groupBy('shop_id');

        $return_array = [];

        foreach ($orders as $key => $order) {
            $arr = [
                'shop' => Shop::where('id', $key)->with('user')->first(),
                'orders' => $order
            ];

            array_push($return_array, $arr);
        }

        return $this->send_response(true, "Order retreived successfully", $return_array);
    }

    public function cancelOrder($code)
    {
        $update = Order::where('code', $code)->update(['status' => 'Cancelled']);

        $pickup = Pickup::where('order_code', $code)->first();
        if ($pickup) {
            $pickup->fill([
                'order_status' => 'Cancelled'
            ]);
        }

        return $this->send_response(true, 'order cancelled succssfully', $update);
    }

    public function update(Request $request, $code)
    {

        $orders = Order::where('code', $code)->get();
        foreach ($orders as $key => $order) {
            $order->status = "Completed";

            $order->save();
        }

        $pickup = Pickup::where('order_code', $code)->first();
        $pickup->order_code = 'Completed';
        $pickup->save();

        return $this->send_response(true, 'Update was successfull');
    }
}
