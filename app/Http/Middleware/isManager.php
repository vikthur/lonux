<?php

namespace Lonux\Http\Middleware;

use Closure;
use Auth;

class isManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('manager')->check()) {
            return $next($request);
        }else{
            return redirect()->route('mLogin');
        }
        // return $next($request);
    }
}
