<?php
require __DIR__.'/../../vendor/laravel/framework/src/Illuminate/Support/Facades/Auth.php';

function lonuxUser()
{
	if (Auth::check()) {
		return Auth::user();
	}
	return null;
}

function lonuxName()
{
	if(!is_null(lonuxUser()))
	{
		return lonuxUser()->name;
	}elseif(!is_null(lonuxManager()))
	{
		return lonuxManager()->name;
	}else{
		return null;
	}

}

function lonuxId()
{
	if(!is_null(lonuxUser()))
	{
		return lonuxUser()->id;
	}elseif(!is_null(lonuxManager()))
	{
		return lonuxManager()->id;
	}else{
		return null;
	}
	
}

function lonuxManager()
{
	if(Auth::guard('manager')->check())
	{
		return Auth::guard('manager')->user();
	}

	return null;
}

function managerShopKey () 
{

	return lonuxManager()->shop_key;

}

function shopId () 
{
	return lonuxManager()->shop()->id;
}

function getPublicId($url)
{
	$oldImageArr = explode('/', $url);

	$count = count($oldImageArr);

	$lastEl = $oldImageArr[$count -1];
	$lastEl = substr($lastEl, 0, -4);

	$public_id = $oldImageArr[$count -2].'/'.$lastEl;

	return $public_id;
}