<?php

namespace Lonux;

class ShopService extends Model
{
    public function shopTypes()
    {
    	return $this->hasMany('Lonux\ShopType');
    }
}
