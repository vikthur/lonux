<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Lonux\Http\Controllers\PhoneNumberVerification;
use Lonux\Http\Controllers\api\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function(){
	Route::prefix('user')->middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

	Route::prefix('business')->group(function(){
			Route::post('/register', [AuthController::class, 'b_register']);
	});

	Route::post('/login', [AuthController::class, 'login']);

	Route::post('/password-reset', [AuthController::class, 'changePassword']);

	Route::prefix('user')->group(function(){
	  	// authentication
	  Route::post('/auth/phone', [PhoneNumberVerification::class, 'phoneVerification']);

		Route::get('/auth/phone/{phone}', [PhoneNumberVerification::class, 'changeNumber']);

		// search
		Route::post('/get_item_shops', 'SearchController@ApiSearch');

		Route::post('/save_user_activity', 'UserController@saveUserActivity');

		Route::post('/save_shop_search', 'UserController@saveShopSearchAppearance');

		Route::get('/get_item_data/{shop_Id}/{item}', 'SearchController@getItemData');

		Route::get('/get_item_suggestions/{query}', 'SearchController@getItemSuggestions');

		Route::get('/get_related_items/{shop_id}/{item}', 'SearchController@getRelatedItems');

		// shop rating
		Route::post('/save_rating', 'MiscController@saveRating');

		Route::post('/test', 'SearchController@testDist');

		// bookings
		Route::resource('booking', 'BookingController');

		// user
		Route::group(['middleware' => 'auth:sanctum'], function(){
			Route::get('/get_stats', 'UserController@getStats');
			Route::get('/get_bookings', 'UserController@getBookings');
			Route::get('/get_activities', 'UserController@getActivities');
		});

	});

	Route::prefix('m')->group(function(){
	  Route::post('/mlogin', 'api\AuthController@mLogin');
	  Route::post('/upc_check', 'manager\InventoryController@getItemByUPC')->middleware('auth:sanctum');
	  Route::post('inventory_store', 'manager\InventoryController@storeInventory')->middleware('auth:sanctum');
	});

});