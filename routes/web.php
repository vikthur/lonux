<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::domain('manager.lonux.com')->group(function(){
// 	Route::get('/', function(){
// 		return view('welcome');
// 	});
// 	Route::resource('manager', "ShopManagerController");
// });

// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Route;
use Lonux\Http\Controllers\CompanyController;
use Lonux\Http\Controllers\OrderController;
use Lonux\Http\Controllers\SearchController;

Route::get('/', function () {
	return view('welcome');
});
Route::get('/getting-started-for-business-owners', function () {
	return view('blog');
})->name('blog');

Auth::routes();

Route::get('/auth/phone', 'PhoneNumberVerification@index');

Route::post('/auth/phone', 'PhoneNumberVerification@phoneVerification');

Route::get('/auth/phone/{phone}', 'PhoneNumberVerification@changeNumber');

Route::view('/resetPassword', 'auth.passwords.customReset');

Route::post('/password-reset', 'PhoneNumberVerification@changePassword');

Route::get('/home', 'HomeController@index')->name('home'); #->middleware('is_manager');

// misc routes start here
Route::get('/get_lonux_countries', 'MiscController@get_lonux_countries');

Route::get('/get_lonux_states/{country}', 'MiscController@get_lonux_states');

Route::get('/get_lonux_lgas/{state}', 'MiscController@get_lonux_lgas');

Route::get('/check_business_name/{name}', 'MiscController@checkBusinessName');
//misc routes end

Route::post('/finish_setup', 'HomeController@finish_setup');

Route::resource('company', 'CompanyController');

Route::post('/company/settings/time', 'CompanyController@changeShopsTime');

Route::get('/business_info', 'CompanyController@getInfo');

Route::get('/get_business_orders', [CompanyController::class, 'getOrders']);

// shop/branch routes
Route::get('/get_shop_types/{id}', 'MiscController@getShopTypes');

Route::get('/get_shop_services', 'MiscController@getShopServices');

Route::post("new_shop", "ShopController@store");

Route::get("getShops", "ShopController@showAll");

Route::get("getShop/{id}", "ShopController@show");

Route::post("add_shop_items", "ShopController@storeNewItems");

Route::get('/get_category_items/{id}', 'ShopController@getCategoryItems');

Route::post('/get_search_results', 'ShopController@getSearchResults');

Route::post('/update_single_shop_item', 'ShopController@updateSingleShopItem');

Route::post('/update_shop_status/{id}', 'ShopController@updateShopStatus');

Route::get('/get_shop_status/{id}', 'ShopController@getShopStatus');

Route::get('/get_what_this_shop_sell/{key}', 'ShopController@getWhatThisShopSell');

Route::post('/update_shop', 'ShopController@UpdateShop');

Route::post('/save_shop_image', 'ShopController@saveShopImage');

Route::post('/shop/settings/time', 'ShopController@changeShopTime');

Route::resource('/manager', 'ShopManagerController');

Route::post('/change_manager_data', 'ShopManagerController@changeManagerData');

Route::get('/stats/get_shops_sales', 'SalesController@getShopsSales');

Route::get('/stats/get_best_selling_items', 'SalesController@getBestSellingItems');

Route::post('/stats/get_charts_data', 'SalesController@getChartsData');

Route::get('/stats/get_seven_days_chart_data', 'SalesController@getSevenDaysChartData');

Route::get('/shop/get_shop_sales/{key}', 'SalesController@getShopSales');

Route::get('/shop/get_shop_best_selling_items/{key}', 'SalesController@getShopBestSellingItems');

Route::get('/shop/get_shop_seven_days_chart_data/{key}', 'SalesController@getShopSevenDaysChartData');

Route::get('/shop/view_shop_sales/{key}', 'SalesController@ViewShopSales');

Route::post('/shop/add_service', 'ServicesController@store');
// end shop/branch routes

// riders
Route::resource('/rider', 'RiderController');


// user dashboad
Route::view('/home/user', 'user.dashboard');
Route::prefix('user')->group(function () {
	Route::get('get_stats', 'UserController@getStats');
	Route::get('get_orders', 'UserController@getBookings');
	Route::get('get_activities', 'UserController@getActivities');
	Route::resource('orders', \OrderController::class);
	Route::post('/store_order', [OrderController::class, 'storeOrder']);
	Route::post('/cancel_orders/{code}', [OrderController::class, 'cancelOrder']);
	Route::get('/directions/{code}', [OrderController::class, 'getDirections']);
});

// search
Route::get('/search', 'SearchController@searchPage');
Route::get('/shop/{key}', [SearchController::class, 'shopPage']);
Route::get('get_shop_items/{key}', [SearchController::class, 'getShopItems']);
Route::get('get_shop_rating/{key}', [SearchController::class, 'getShopRating']);
Route::get('cart', [SearchController::class, 'cart']);

Route::get('/result/{key}', 'SearchController@resultPage');

Route::post('/get_item_shops', 'SearchController@getItemShops');

Route::get('/get_item_data/{shop_Id}/{item}', 'SearchController@getItemData');

Route::get('/get_item_suggestions/{query}', 'SearchController@getItemSuggestions');

Route::get('/get_related_items/{shop_id}/{item}', 'SearchController@getRelatedItems');


// user activity
Route::post('/save_user_activity', 'UserController@saveUserActivity');
Route::post('/save_shop_search', 'UserController@saveShopSearchAppearance');

// shop rating
Route::post('/save_rating', 'MiscController@saveRating');

// add inventory business owners
Route::get('/b/get_items_categories', 'manager\InventoryController@getItemsCategory');
Route::post('/b/add_inventory', 'manager\InventoryController@storeInventory');

// shop admins/managers
Route::group(['middleware' => 'is_manager'], function () {
	Route::get('/m/manage-inventory', "manager\HomeController@manageInventory");
	Route::get('/m/new-inventory', "manager\HomeController@newInventory");
	Route::get('/m/settings', "manager\HomeController@settings");
	Route::get('/m', "manager\HomeController@index");

	// store inventory
	Route::post('/m/add_inventory', 'manager\InventoryController@storeInventory');
	Route::get('/m/get_items_categories', 'manager\InventoryController@getItemsCategory');
	Route::get('/m/get_latest_inventory', 'manager\InventoryController@getLatestInventory');

	// view/manage inventory
	Route::get('/m/get_shop_items_category', 'manager\InventoryController@getShopItemsCategory');
	Route::get('/m/manage-inventory/{id}/{name}', "manager\HomeController@getCategoryItems");
	Route::get('/m/get_category_items/{id}', 'manager\InventoryController@getCategoryItems');
	Route::get('/m/get_inventory-item/{id}', 'manager\InventoryController@getInventoryItem');
	Route::get('/m/show-inventory-item/{id}/{name}', 'manager\HomeController@showItem');
	Route::post('m/get_item_by_upc', 'manager\InventoryController@getItemByUPC');
	Route::get('/m/bookings', function () {
		return view('manager.bookings');
	});

	// update items
	Route::post('/m/item_is_out_of_stock', 'manager\InventoryController@itemIsOutOfStock');
	Route::post('m/update_inventory', 'manager\InventoryController@updateInventory');
	Route::get('/m/edit_item/{id}/{name}', 'manager\HomeController@editItem');
	Route::post('/m/delete_item', 'manager\InventoryController@deleteItem');

	// bookings
	Route::get('/m/get_bookings', 'manager\InventoryController@getBookings');
	Route::get('/m/show-booking/{id}/{name}', 'manager\HomeController@showBooking');
	Route::get('/m/get_booked_item/{id}', 'manager\InventoryController@getBookedItem');
	Route::post('/m/item_was_picked', 'manager\InventoryController@itemWasPicked');
	Route::post('/m/customer_didnt_come', 'manager\InventoryController@customerDidntCome');
	Route::post('/m/search_booking', 'manager\InventoryController@searchBooking');
	Route::post('/file/change_this_image', 'manager\InventoryController@changeThisImage');
	Route::post('file/add_new_image', 'manager\InventoryController@addNewImage');
	Route::post('file/delete_image', 'manager\InventoryController@deleteImage');
	// push notification
	Route::post('/m/subscribe_manager', 'manager\HomeController@subscribeManager');

	// shop 
	Route::post('/m/update_shop_status/{id}', 'manager\HomeController@updateShopStatus');
	Route::get('/m/get_shop_status/{id}', 'manager\HomeController@getShopStatus');
});

Route::get('/m/login', 'manager\Auth\LoginController@showLoginForm')->name('mLogin');
Route::post('/m/login', 'manager\Auth\LoginController@login');
Route::post('/m/logout', 'manager\Auth\LoginController@login');
//shop admins/managers

// bookings
Route::resource('booking', 'BookingController');

// admin dashboard
Route::group(['prefix' => 'adminportal'], function () {
	Route::view('/', 'admin.index');
	Route::view('/users', 'admin.users');
});

// waitlist
Route::post('waitlist', 'MiscController@addWaiter')->name('waitlist');

Broadcast::routes();

// Route::get('/user/{vue_capture?}', function () {
//     return view('user.dashboard');
// })->where('vue_capture', '[\/\w\.-]*')->middleware('auth');

Route::get('/{vue_capture?}', function () {
	return view('pages.home');
})->where('vue_capture', '[\/\w\.-]*')->middleware('auth');
